section .text
%define SYS_EXIT 60 
%define SYS_READ 0
%define SYS_WRITE 1
%define FD_RD 0
%define FD_WR 1
%define BASE 10
%define NEW_LINE 0xA
%define TAB 0x9
%define SPACE 0x20

exit: 
    mov rax, SYS_EXIT
    syscall

string_length:
    xor rax, rax
.L:
    cmp byte [rdi+rax], 0
    je .E
    inc rax
    jmp .L
.E:
    ret

print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax 
    mov rax, SYS_WRITE
    mov rdi, FD_WR 
    syscall
    mov rdi, rsi
    ret


print_newline:
    mov rdi, NEW_LINE

print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret

string_equals:
	xor rax, rax
.L:
	mov dl, byte[rdi+rax]
   	cmp byte[rsi + rax], dl
    jne .NE
    inc rax
    cmp dl, 0
    jne .L
    mov rax, 1
    ret
.NE:
    xor rax, rax
    ret

string_copy:
    call string_length
    cmp rdx, rax
    jbe .SB
    push rax
.L: 
    mov dl, byte[rdi]
    mov byte[rsi], dl
    inc rdi
    inc rsi
    cmp dl, 0
    jnz .L 
    pop rax 
    ret
.SB:
    xor rax, rax
    ret

print_int:
    test rdi, rdi
    jns .A
    jmp .B
.A:
    call print_uint
    ret
.B:
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
	call print_uint
	ret

print_uint:
    mov rax, rdi
    mov rdi, rsp
    sub rsp, 24
    dec rdi
    mov r8, BASE
.L:
    xor rdx, rdx
    div r8
    or  dl, 0x30
    dec rdi 
    mov [rdi], dl
    test rax, rax
    jnz .L 
    call print_string
    add rsp, 24
    ret    



read_char:    
    xor rax, rax 
    xor rdi, rdi
    push 0 
    mov rsi, rsp 
    mov rdx, 1
    syscall
    pop rax
    ret

read_word:
	xor rcx, rcx 
	mov r8, rdi
	mov r9, rsi
.L:	
	push rcx
	call read_char
	pop rcx
	cmp rax, SPACE
	je .BW
	cmp rax, TAB
	je .BW
	cmp rax, NEW_LINE
	je .BW
	cmp rax, 0
	jz .END
	cmp rcx, r9
	jnl .E
	mov byte[r8+rcx], al
	inc rcx
	jmp .L
.BW:
	cmp rcx, 0
	je .L
.END:	
	mov byte[r8+rcx], 0
	mov rax, r8
	mov rdx, rcx
	ret 
.E:
	xor rax, rax
	ret

parse_uint:
    mov r8, BASE
    xor rax, rax
    xor rcx, rcx
.L:
    movzx rsi, byte [rdi + rcx] 
    cmp sil, '0'
    jb .E
    cmp sil, '9'
    ja .E
    xor rdx, rdx 
    mul r8
    and sil, 0x0f
    add rax, rsi
    inc rcx 
    jmp .L
.E:
    mov rdx, rcx
    ret


parse_int:
    mov al, byte [rdi]
    cmp al, '-'
    je .A
    call parse_uint
    ret
.A:
    inc rdi
    call parse_uint
    neg rax
    test rdx, rdx
    jz .B
    inc rdx
    ret
.B:
    xor rax, rax
    ret 

